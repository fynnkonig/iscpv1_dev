#pylint: disable=logging-too-many-args
#pylint: disable=unused-wildcard-import
import globals
from packets import *
import math

class Process:
    def __init__(self, device, starttime):
        self.device = device
        self.starttime = starttime
        self.running = True
        self.device.runningProcesses.append(self)
        self.activeFunctions = []

    def timeUpdate(self):
        for f in self.activeFunctions:
            f()

    def endProcess(self):
        functions = [func for func in self.activeFunctions]
        for f in functions:
            self.activeFunctions.remove(f)
        self.device.runningProcesses.remove(self)
        self.running = False

class AudioTransmission(Process):
    def __init__(self, device, server, starttime, length):
        super().__init__(device, starttime)
        self.type = "AudioTransmission"
        self.transmittAudioUntil = starttime + length
        self.sendNextAudioPacketAt = starttime
        self.server = server
        self.activeFunctions.append(self.transmittAudio)

    def transmittAudio(self):
        if globals.TIME >= self.transmittAudioUntil:
            globals.logger.log(self.device.ip, "finished Sending Audio.")
            self.endProcess()
        elif globals.TIME >= self.sendNextAudioPacketAt:
            self.sendNextAudioPacketAt += 1
            globals.logger.log(self.device.ip, "sending 1s of Audio to Server")
            audiodata = AudioData()
            audiodata.sendTo(self.server, self.device)

class HeartbeatListener(Process):
    #server only!
    def __init__(self, server, client, starttime, offset=0):
        super().__init__(server, starttime)
        self.type = "HeartbeatListener"
        self.server = server
        self.client = client
        self.interval = 3 #standard-Interval
        self.clientAliveUntil = self.starttime + self.interval - offset
        self.activeFunctions.append(self.timeUpdate)

    def timeUpdate(self):
        if self.clientAliveUntil <= globals.TIME:
            globals.logger.log("Server", self.server.ip, ": Client", self.client.ip, "died. Notifying Clients.")
            self.reset()
            self.alarmClientsAndUpdateServer()
        
    def reset(self):
        self.clientAliveUntil = globals.TIME + self.interval

    def alarmClientsAndUpdateServer(self):
        self.server.connectedClients.remove(self.client)
        for client in self.server.connectedClients:
            ReportMissingClient(self.server, globals.TIME, self.client, client)
        self.endProcess()

class ReportMissingClient(Process):
    def __init__(self, server, starttime, missingClient, targetClient):
        super().__init__(server, starttime)
        self.type = "ReportMissingClient"
        self.missingClient = missingClient
        self.targetClient = targetClient
        self.packet = MISS([self.missingClient])
        self.packet.sendTo(self.targetClient, self.device)
        self.waitInterval = 1
        self.waitForAckUntil = starttime + self.waitInterval
        self.activeFunctions.append(self.waitForAckAndResend)
        globals.logger.log("     notified", self.targetClient.ip, "waiting for ACK.")

    def waitForAckAndResend(self):
        if self.waitForAckUntil <= globals.TIME:
            self.waitForAckUntil = globals.TIME +1
            globals.logger.log("Server", self.device.ip, "didnt receive Miss-Acknowledgement from", self.targetClient.ip, "in Time. Resending MISS.")
            self.packet.sendTo(self.targetClient, self.device)

    def receiveMiss(self, packet):
        globals.logger.log("Server", self.device.ip, "received MISS-ACK from", packet.senderClient.ip)
        self.endProcess()

def printTime():
    if math.floor(globals.TIME) != math.floor(globals.TIME - globals.TIME_STEP_SIZE):
        print("Time", math.floor(globals.TIME), "s")