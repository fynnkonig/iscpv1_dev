#pylint: disable=logging-too-many-args
#pylint: disable=unused-wildcard-import
from packets import *
from copy import copy
import globals
import process

class Device:
    def __init__(self, ip):
        self.ip = ip
        self.receivedPackets = []
        self.runningProcesses = []
        self.packetTimers = []
        self.alive = True

    def timeUpdate(self):
        for process in self.runningProcesses:
            process.timeUpdate()
        for timer in self.packetTimers:
            timer.tick()
    
    def receive(self, packet):
        #implement in subclasses
        pass

    def die(self):
        self.alive = False
        self.packetTimers = []
        self.runningProcesses = []

    def reset(self):
        self.die()
        self.alive = True
        self.receivedPackets = []

class Server(Device):
    def __init__(self, ip):
        super().__init__(ip)
        self.type = "Server"
        self.connectedClients = []

    def connectClient(self, client, offset=0):
        process.HeartbeatListener(self, client, globals.TIME, offset)
        self.connectedClients.append(client)

    def getHeartbeatListener(self, client):
        for p in self.runningProcesses:
            if p.type == "HeartbeatListener" and p.client == client:
                return p
        return

    def getMissProcess(self, missingClient, targetClient):
        for p in self.runningProcesses:
            if p.type == "ReportMissingClient" and p.missingClient == missingClient and p.targetClient == targetClient:
                return p
        return

    def receive(self, packet):
        if not self.alive:
            pass
        elif packet.type == "ALVE":
            # globals.logger.log("Server", self.ip, "received ALVE from", packet.senderClient.ip)
            p = self.getHeartbeatListener(packet.senderClient)
            if p:
                p.reset()
        elif packet.type == "MISS":
            p = self.getMissProcess(packet.missingClients[0], packet.senderClient)
            if p:
                p.receiveMiss(packet)
            else:
                globals.logger.log("An Error occured, Server received MISS with no process running!")
        elif packet.type == "STRX":
            globals.logger.log("Server", self.ip, "received STRX from", packet.senderClient.ip, "and forwarded to clients.")
            for client in self.connectedClients:
                strx = copy(packet)
                strx.sendTo(client, self)
                globals.logger.log("     Forwarding to", client.ip)
        elif packet.type == "AudioTx":
            globals.logger.log("Server", self.ip, "received 1s of Audio from", packet.senderClient.ip, "and forwarded to clients.")
            for client in self.connectedClients:
                if client != packet.senderClient:
                    audiodata = copy(packet) #might this be a problem with the python garbage collection?
                    audiodata.sendTo(client, self)
        else:
            raise Exception("Server received unknown packet of type " + packet.type)

class Client (Device):
    def __init__(self, ip, server=None):
        super().__init__(ip)
        self.type = "Client"
        self.server = server

    def connect(self, server=None, offset=0):
        if not server and self.server:
            self.server.connectClient(self, offset)
        elif server:
            server.connectClient(self, offset)
        else:
            raise Exception("Couldnt connect to Server. No valid Server given.")
        alive = ALVE()
        self.packetTimers.append(PacketTimer(1, offset, alive, self, self.server))

    def simulateAudioTransmission(self, groupId, length):
        globals.logger.log(self.ip, "starting Audio Transmission for", length, "seconds in partyline.")
        strx = STRX(0)
        strx.sendTo(self.server, self)
        process.AudioTransmission(self, self.server, globals.TIME, length)

    def receive(self, packet):
        if not self.alive:
            pass
        elif packet.type == "MISS":
            globals.logger.log(self.ip, "received MISS, returning MISS for Ackknowledgement.")
            p = copy(packet)
            p.sendTo(packet.senderClient, self)
        elif packet.type == "STRX":
            globals.logger.log(self.ip, "received STRX, waiting for Audio...")
        elif packet.type == "AudioTx":
            globals.logger.log(self.ip, "received 1s of Audio.")
        else:
            raise Exception("Client received unknown packet of type " + packet.type)

class PacketTimer:
    def __init__(self, interval, timeroffset, packet, client, toClient):
        self.interval = interval
        self.packet = packet
        self.client = client
        self.toClient = toClient
        self.time = interval - timeroffset

    def setRandomTime(self):
        self.time = random.random() + globals.TIME_STEP_SIZE * (self.interval - globals.TIME_STEP_SIZE)

    def tick(self):
        self.time -= globals.TIME_STEP_SIZE
        if self.time <= 0:
            packet = copy(self.packet)
            packet.sendTo(self.toClient, self.client)
            self.resetTimer()

    def resetTimer(self):
        self.time = self.interval