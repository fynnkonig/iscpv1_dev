import globals

class Logger:
    def __init__(self, filename, writeFile = True, printLines = True, logWithTimestamp = True):
        self.file = open(filename, "w+")
        self.writeFile = writeFile
        self.print = printLines
        self.logWithTimestamp = logWithTimestamp
    
    def log(self, *args):
        ts = str(round(globals.TIME, 3))
        msg = ts + " " if self.logWithTimestamp else ""
        for s in args:
            msg += str(s) + " "
        if self.print:
            print(msg)
        if self.writeFile:
            self.file.write(msg + "\n")

    def initialLog(self, *args):
        msg = ""
        for s in args:
            msg += str(s) + " "
        if self.print:
            print(msg)
        if self.writeFile:
            self.file.write(msg + "\n")

    def __del__(self):
        self.file.close()

class CSVLogger(Logger):
    def __init__(self, filename, headings, writeFile = True, printLines = True, logWithTimestamp = True, timestampPrecision = 3):
        super().__init__(filename, writeFile, printLines)
        self.headings = headings
        self.logWithTimestamp = logWithTimestamp
        self.timestampPrecision = timestampPrecision
        self.writeHeadings()

    def log(self, *args):
        if len(args) == len(self.headings):
            msg = "" if not self.logWithTimestamp else str(round(globals.TIME, self.timestampPrecision)) + ","
            for i in range(len(args)):
                msg += str(args[i])
                if i < len(args) - 1:
                    msg += ","
            if self.print:
                print(msg)
            if self.writeFile:
                self.file.write(msg + "\n")
        else:
            raise Exception("Missing or too much arguments for CSVLogger.log.")

    def writeHeadings(self):
        line = ""
        if self.logWithTimestamp:
            line += "Timestamp,"
        for i in range(len(self.headings)):
            line += self.headings[i]
            if i < len(self.headings) - 1:
                line += ","
        if self.print:
            print(line)
            pass
        if self.writeFile:
            self.file.write(line + "\n")

class NetworkTrafficLogger:
    def __init__(self, starttime, writeFiles):
        self.loggers = []
        self.clientTraffic = []
        self.regularLogFunctions = []
        self.totalTraffic = 0
        self.trafficDuringLastSecond = 0
        self.nextUpdateTime = starttime + 1.0
        self.writeFiles = writeFiles

    def watchTotalNetworkBandwidth(self):
        filename = globals.SIM_NAME + "TotalBandwidth.csv"
        self.loggers.append({"client" : None, "logger"  : CSVLogger(filename, ["BandwidthPerSec", "TotalBandwidth"], printLines=False, writeFile = self.writeFiles, timestampPrecision=0)})
        if self.update not in self.regularLogFunctions:
            self.regularLogFunctions.append(self.update)

    def watchBandwithForClient(self, client):
        filename = globals.SIM_NAME + "client_" + client.ip + "_TotalBandwidth.csv"
        self.loggers.append({"client" : client, "logger" : CSVLogger(filename, ["TXperSec", "TXTotal", "RXperSec", "RXTotal"], printLines=False, writeFile = self.writeFiles, timestampPrecision=0)})
        if self.update not in self.regularLogFunctions:
            self.regularLogFunctions.append(self.update)
        self.clientTraffic.append({"client" : client, "TXps" : 0, "TXTotal" : 0, "RXps" : 0, "RXTotal" : 0})

    def logPacketTransmission(self, packet):
        self.totalTraffic += packet.size
        self.trafficDuringLastSecond += packet.size
        for entry in self.clientTraffic:
            if entry["client"] == packet.senderClient:
                entry["TXps"] += packet.size
                entry["TXTotal"] += packet.size
            if packet.tx == "broadcast" or entry["client"] == packet.targetClient:
                entry["RXps"] += packet.size
                entry["RXTotal"] += packet.size

    def timeUpdate(self):
        for f in self.regularLogFunctions:
            f()

    def update(self):
        if globals.TIME >= self.nextUpdateTime:
            self.updateTotalTraffic()
            self.updateClientTraffic()
            self.nextUpdateTime += 1.0

    def updateTotalTraffic(self):
        logger = self.getLogger(None)
        logger.log(self.trafficDuringLastSecond, self.totalTraffic)
        self.trafficDuringLastSecond = 0
    
    def updateClientTraffic(self):
        for entry in self.clientTraffic:
            logger = self.getLogger(entry["client"])
            logger.log(entry["TXps"], entry["TXTotal"], entry["RXps"], entry["RXTotal"])
            entry["TXps"] = 0
            entry["RXps"] = 0

    def getLogger(self, client):
        for entry in self.loggers:
            if entry["client"] == client:
                return entry["logger"]
        return