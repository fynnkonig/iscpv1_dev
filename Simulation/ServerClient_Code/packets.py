import random
import string
import globals

class Packet:
    def __init__(self):
        self.size = globals.ISCP_HEADER_SIZE
        self.type = ""

    def sendTo(self, targetClient, senderClient):
        self.targetClient = targetClient
        self.tx = "unicast"
        self.senderClient = senderClient
        self.receiveTime = globals.TIME + Packet.getRandomTravelTime()
        #implement logic here
        globals.networktracker.logPacketTransmission(self)
        getLost = random.random() * 100 <= globals.PACKET_LOSS_PROBABILLITY
        if not getLost:
            globals.TRAVELING_PACKETS.append(self)
        else:
            raise Exception("Timeout")
    
    def sendBroadcast(self, packetcount, senderClient):
        self.receiveTime = globals.TIME + Packet.getRandomTravelTime()
        self.tx = "broadcast"
        self.senderClient = senderClient
        globals.networktracker.logPacketTransmission(self)
        globals.TRAVELING_PACKETS.append(self)

    def receive(self):
        if self.tx == "unicast":
            self.receiveSinglePacket(self.targetClient)
        elif self.tx == "broadcast":
            exception = False
            for client in globals.clients:
                try:
                    self.receiveSinglePacket(client)
                except Exception as e:
                    exception = e
            if exception:
                raise exception
        else:
            raise Exception("Unknown Packet Type")

    def receiveSinglePacket(self, client):
        if client.alive:
            client.receive(self)
        else:
            #Add Extra Packets if Timeout and send again applies
            # raise Exception(self.senderClient.ip + ": ERROR: Couldnt reach Client at " + client.ip + ". Packet lost: " + self.type + ".")
            pass
    
    @staticmethod
    def getRandomTravelTime():
        return random.uniform(globals.MIN_TRAVEL_TIME, globals.MAX_TRAVEL_TIME)

class SRCH(Packet):
    def __init__(self):
        self.type = "SRCH"
        self.size = 11
        self.id = random.randint(0, 65535)

class SACK(Packet):
    def __init__(self, id):
        self.type = "SACK"
        self.size = 11
        self.id = id

class RPRT(Packet):
    def __init__(self, name):
        self.type = "RPRT"
        self.size = 29
        if len(name) <= 20:
            self.name = name + " " * (20 - len(name))
        else:
            globals.logger.log("Bad Name-Format:", name)

class IDNT(Packet):
    def __init__(self):
        self.type = "IDNT"
        self.size = 41
        # THIS IS NOT SAFE! ONLY USE FOR SIMULATION!
        self.rnd = "".join(random.choice(string.ascii_lowercase) for i in range (32))

class AUTH(Packet):
    def __init__(self, hash):
        self.type = "AUTH"
        self.size = 25
        self.hash = hash

class KEYX(Packet):
    def __init__(self, key):
        self.type = "KEYX"
        self.size = 520
        self.key = key

class LGIN(Packet):
    def __init__(self, username, password):
        self.type = "LGIN"
        self.size = 1033
        self.username = username
        self.password = password

class ADDP(Packet):
    def __init__(self, cId, number, uIds, ips, deviceIds):
        self.type = "ADD+"
        self.size = globals.ISCP_HEADER_SIZE + 4 + number * 31
        self.cId = cId
        self.uIds = uIds
        self.ips = ips
        self. deviceIds = deviceIds

class RMVE(Packet):
    def __init__(self, clientlist, cId, instantReturn):
        self.type = "RMVE"
        self.size = globals.ISCP_HEADER_SIZE + 4 + len(clientlist) * 31
        self.clientlist = clientlist
        self.cId = cId
        self.instantReturn = instantReturn

class UPDT(Packet):
    def __init__(self, cId):
        self.type = "UPDT"
        self.size = 11
        self.cId = cId

class UACK(Packet):
    def __init__(self, cId):
        self.type = "UACK"
        self.size = 11
        self.cId = cId

class COLL(Packet):
    def __init__(self):
        self.type = "COLL"
        self.size = 9
        
class CONN(Packet):
    def __init__(self, metrics):
        self.type = "CONN"
        self.size = 11 + len(metrics) * 34
        self.metrics = metrics

class MESH(Packet):
    def __init__(self, mesh):
        self.type = "MESH"
        self.size = 13 + len(mesh.connections) * 30
        self.mesh = mesh

class MACK(Packet):
    def __init__(self, id):
        self.type = "MACK"
        self.size = 11
        self.id = id
    
class PING(Packet):
    def __init__(self, return_1, return_2, respond, alive, timestamp):
        self.type = "PING"
        self.size = 14
        self.return_1 = return_1
        self.return_2 = return_2
        self.respond = respond
        self.alive = alive
        self.timestamp = timestamp #This is a random Pseudo Timestamp to identify, which Packet is a Response to which Ping
        if alive:
            self.pingtype = "Alive"
        elif not return_1 and not return_2:
            self.pingtype = "Initial"
        elif return_1:
            self.pingtype = "Return 1"
        elif return_2:
            self.pingtype = "Return 2"
        if respond:
            self.pingtype += " respond"

class STOP(Packet):
    def __init__(self, client):
        self.type = "STOP"
        self.size = 30 + 1 * 10
        self.client = client

class MISS(Packet):
    def __init__(self, missingClients):
        self.type = "MISS"
        self.size = 14 + len(missingClients) * 20
        self.missingClients = missingClients

class UCTX(Packet):
    def __init__(self, txid, seq_nr, seq_size, length, ids, roles, pws, prios):
        self.type = "UCTX"
        self.size = 17 + length * 40
        self.seq_nr = seq_nr
        self.seq_size = seq_size
        self.length = length
        self.ids = ids
        self.roles = roles
        self.pws = pws
        self.prios = prios

class IGTX(Packet):
    def __init__(self, txid, seq_nr, seq_size, length, ids, names, list_num_of_clients, cIds):
        self.type = "IGTX"
        num_of_client_fields = 0
        for num_of_clients in list_num_of_clients:
            num_of_client_fields += num_of_clients
        self.size = 17 + length * 24 + 2 * num_of_client_fields
        self.seq_nr = seq_nr
        self.seq_size = seq_size
        self.length = length
        self.ids = ids
        self.names = names
        self.list_num_of_clients = list_num_of_clients
        self.cIds = cIds

class ACTX(Packet):
    def __init__(self, txid, seq_nr, seq_size, length, uIds, ips, devices, mobile):
        self.type = "ACTX"
        self.size = 17 + length * 11
        self.seq_nr = seq_nr
        self.seq_size = seq_size
        self.length = length
        self.uIds = uIds
        self.ips = ips
        self.devices = devices
        self.mobile = mobile

class NVTX(Packet):
    def __init__(self, txid, publicIdentifier, secretIdentifier):
        self.type = "NVTX"
        self.size = 159
        self.txid = txid
        self.publicIdentifier = publicIdentifier
        self.secretIdentifier = secretIdentifier

class TACK(Packet):
    def __init__(self, txid, seq_nr):
        self.type = "TACK"
        self.size = 13
        self.txid = txid
        self.seq_nr = seq_nr

class ALVE(Packet):
    def __init__(self):
        self.type = "ALVE"
        self.size = 9

class CAPT(Packet):
    def __init__(self, group):
        self.type = "CAPT"
        self.size = 11
        self.group = group

class STRX(Packet):
    def __init__(self, groupId):
        # length should be the size of the sdp information in bytes!
        self.type = "STRX"
        self.size = 191
        self.groupId = groupId

class AudioData(Packet):
    # simulates the Transfer of 1s of 64kbit/s encoded Opus Audio over RTP
    def __init__(self):
        self.type = "AudioTx"
        self.size = int(49000 / 5)