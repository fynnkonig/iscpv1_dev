class Timeline:
    # The way this is implemented you cant go back in time, as old elements get deleted!
    def __init__(self):
        self.actions = []

    def addAction(self, function, starttime, duration, *args):
        self.actions.append({
            "function" : function,
            "params" : args,
            "starttime" : starttime,
            "duration" : duration
            })

    def timeUpdate(self, time):
        actions = [action for action in self.actions]
        for action in actions:
            if action["starttime"] == time or action["starttime"] < time and time < action["starttime"] + action["duration"]:
                fdict = zip([action["function"]], [action["params"]])
                for f, args in fdict:
                    f(*args)
                self.actions.remove(action)