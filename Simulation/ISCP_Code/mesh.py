from logging import Logger
import globals

class Mesh:
    def __init__(self, groupId):
        self.groupId = groupId
        self.connections = []
        self.clients = []

    def addConnection(self, fromClient, toClient, aliveoffset=0):
        if not self.directConnectionExistsBetween(fromClient, toClient):    
            self.connections.append(Connection(fromClient, toClient, aliveoffset))
            for client in [fromClient, toClient]:
                if client not in self.clients:
                    self.clients.append(client)

    def addConnections(self, fromClients, toClients):
        if len(fromClients) != len(toClients):
            raise Exception("List fromClients has a different Length than toClients. Couldnt continue.")
        else:
            for i in range(len(fromClients)):
                self.addConnection(fromClients[i], toClients[i])

    def getNeighbours(self, client):
        neighbours = []
        for connection in self.connections:
            if connection.contains(client):
                neighbours.append(connection.getConnectedClient(client))
        return neighbours

    def connectMesh(self):
        for connection in self.connections:
            connection.connect()
    
    def directConnectionExistsBetween(self, client1, client2):
        for connection in self.connections:
            if connection.isBetween(client1, client2):
                return True
        return False

    def removeClient(self, client):
        if client in self.clients:
            connections = [connection for connection in self.connections]
            for connection in connections:
                if connection.contains(client):
                    self.connections.remove(connection)
            self.clients.remove(client)

    def getClientId(self, client):
        for i in range(len(self.clients)):
            if self.clients[i] == client:
                return i
        return None

    def duplicate(self):
        newMesh = Mesh(self.groupId)
        for connection in self.connections:
            newMesh.connections.append(connection)
            for client in [connection.fromClient, connection.toClient]:
                if client not in newMesh.clients:
                    newMesh.clients.append(client)
        return newMesh

    def connectionWouldCreateLoop(self, fromClient, toClient):
        return fromClient in self.clients and toClient in self.clients and self.clientsAreConnected(fromClient, toClient)
    
    def clientsAreConnected(self, client1, client2):
        subtrees = self.buildSubTrees()
        for tree in subtrees:
            if tree.containsBoth(client1, client2):
                return True
        return False

    def buildSubTrees(self):
        subtrees = []
        for connection in self.connections:
            connectedTree = None
            for tree in subtrees:
                if tree.containsClientOf(connection):
                    if connectedTree == None:
                        connectedTree = tree
                        tree.addConnection(connection)
                    else:
                        tree.mergeTrees(connectedTree, subtrees)
            if connectedTree == None:
                subtrees.append(Tree(connection))
        return subtrees

    def writeFile(self):
        filename = globals.SIM_NAME + "mesh" + str(globals.num_of_pass) + ".txt"
        globals.num_of_pass += 1
        logger = Logger(filename, printLines = False, logWithTimestamp = False)
        for conn in self.connections:
            logger.log("Connection:", conn.fromClient.ip, "to", conn.toClient.ip)

class Connection:
    def __init__(self, fromClient, toClient, aliveoffset=0):
        if fromClient == toClient:
            raise Exception("Cannot connect a Client to itself!")
        self.fromClient = fromClient
        self.toClient = toClient
        self.clients = [fromClient, toClient]
        self.aliveoffset = aliveoffset

    def contains(self, client):
        return self.fromClient == client or self.toClient == client

    def getConnectedClient(self, client):
        if client == self.fromClient:
            return self.toClient
        elif client == self.toClient:
            return self.fromClient
        else:
            raise Exception("Client is not part of this Connection!")
    
    def connect(self):
        self.fromClient.connect(self.toClient, self.aliveoffset)

    def isBetween(self, client1, client2):
        return client1 != client2 and client1 in self.clients and client2 in self.clients

class Tree:
    def __init__(self, connection):
        self.clients = [connection.fromClient, connection.toClient]

    def addConnection(self, connection):
        for client in [connection.fromClient, connection.toClient]:
            self.clients.append(client)

    def mergeTrees(self, tree, list_of_trees):
        for client in tree.clients:
            if client not in self.clients:
                self.clients.append(client)
        list_of_trees.remove(tree)

    def containsClientOf(self, connection):
        return connection.fromClient in self.clients or connection.toClient in self.clients

    def containsBoth(self, client1, client2):
        return client1 in self.clients and client2 in self.clients