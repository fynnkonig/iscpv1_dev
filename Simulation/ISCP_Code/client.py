#pylint: disable=logging-too-many-args
#pylint: disable=unused-wildcard-import
from packets import *
from copy import copy
import globals
import process

class Client:
    def __init__(self, ip, mesh):
        self.ip = ip
        self.idle = Idle(self)
        self.state = self.idle
        self.packetTimers = []
        self.actionTimers = []
        self.receivedPackets = []
        self.runningProcesses = []
        self.connectedClients = []
        self.alive = True
        self.canStartUpdate = True
        self.meshes = [mesh]
        self.oldProcesses = []
    
    def receive(self, packet):
        self.state.receive(packet)

    def changeState(self, newState):
        self.state = newState
        self.onStateChange()

    def onStateChange(self):
        pass

    @property
    def canStartUpdate(self):
        return self.__canStartUpdate

    @canStartUpdate.setter
    def canStartUpdate(self, canStartUpdate):
        if canStartUpdate == False:
            self.__canStartUpdate = False
            self.actionTimers.append(ActionTimer(5, 0, "resetStopUpdate", None, self))
            #Can you reset the timer if you see that the update makes progress?
        else:
            self.__canStartUpdate = True

    def timeUpdate(self):
        for timer in self.packetTimers:
            timer.tick()
        for timer in self.actionTimers:
            if timer.tick():
                if timer.action == "ALVE" and self.canStartUpdate:
                    globals.logger.log(self.ip + ": Client", timer.forClient.ip, "died. Trying to initiate Update.")
                    self.initiateUpdate(timer.forClient)
                    timer.resetTimer()
                elif timer.action == "ALVE" and not self.canStartUpdate:
                    timer.resetTimer()
                    globals.logger.log(self.ip, "couldnt start Update. There should be one active right now.")
                elif timer.action == "resetStopUpdate":
                    self.canStartUpdate = True
                else:
                    raise Exception("Unknown ActionTimer Format")
        for process in self.runningProcesses:
            process.timeUpdate()

    def connect(self, client, timeroffset=0):
        if client not in self.connectedClients:
            self.actionTimers.append(ActionTimer(4, timeroffset, "ALVE", client, self))
            client.actionTimers.append(ActionTimer(4, timeroffset, "ALVE", self, client))
            self.packetTimers.append(PacketTimer(1,timeroffset %1,  ALVE(), self, client))
            client.packetTimers.append(PacketTimer(1, timeroffset %1, ALVE(), client, self))
            self.connectedClients.append(client)
            client.connectedClients.append(self)

    def initiateUpdate(self, client):
        process.WaitForMissAndStop(self, globals.TIME)
        stop_packet = STOP(client)
        stop_packet.sendBroadcast(3, self)
        
    def die(self):
        self.alive = False
        self.packetTimers = []
        self.actionTimers = []
        self.runningProcesses = []
        globals.logger.log("CLIENT DIED:", self.ip)

    def reset(self):
        self.die()
        self.alive = True
        self.receivedPackets = []

    def getPacketsOfType(self, packettype):
        return [packet for packet in self.receivedPackets if packet.type == packettype]

    @property
    def neighbours(self):
        return self.meshes[0].getNeighbours(self)

    def isLeaveMember(self, mesh):
        return len(mesh.getNeighbours(self)) == 1

    def isCoreMember(self, mesh):
        return len(mesh.getNeighbours(self)) > 1

    def getProcessByType(self, ptype):
        for process in self.runningProcesses:
            if process.type == ptype:
                return process
        return

    def clearTimers(self, mesh):
        globals.logger.log(self.ip, "cleared Timers and stopped Heartbeat sending and recognition.")
        packetTimers = [pt for pt in self.packetTimers]
        for pt in packetTimers:
            if pt.toClient in mesh.clients:
                self.packetTimers.remove(pt)
        actionTimers = [pt for pt in self.actionTimers]
        for at in actionTimers:
            if at.forClient in mesh.clients:
                self.actionTimers.remove(at)
        for client in self.neighbours:
            if client in self.connectedClients:
                self.connectedClients.remove(client)

    def pauseTimers(self):
        for timer in self.actionTimers:
            timer.pause()

    def playTimers(self):
        for timer in self.actionTimers:
            timer.play()

    def simulateAudioTransmission(self, groupId, length):
        globals.logger.log(self.ip, "starting Audio Transmission for", length, "seconds in partyline.")
        groupId = 0 #no Groups Implemented yet!
        neighbours = self.meshes[groupId].getNeighbours(self)
        txid = random.randint(0, 65535)
        for neighbour in neighbours:
            strx = STRX(groupId, txid)
            strx.sendTo(neighbour, self)
            process.WaitForTXAK(self, globals.TIME, strx, 0.1, neighbour)
        process.AudioTransmission(self, neighbours, globals.TIME, length)

class Idle:
    def __init__(self, client):
        self.client = client
        self.ip = client.ip #only for better debugging overview!
    
    def receive(self, packet):
        if not self.client.alive:
            pass
        elif packet.type == "ALVE":
            # globals.logger.log(self.client.ip, "received ALVE-Heartbeat from", packet.senderClient.ip)
            for timer in self.client.actionTimers:
                if timer.action == "ALVE" and timer.forClient.ip == packet.senderClient.ip:
                    timer.resetTimer()
            packets = [packet for packet in self.client.receivedPackets]
            for oldPacket in packets:
                if oldPacket.type == "ALVE" and oldPacket.receiveTime < globals.TIME - 4:
                    self.client.receivedPackets.remove(oldPacket)
            self.client.receivedPackets.append(packet)
        elif packet.type == "SRCH":
            sack = SACK(packet.id)
            sack.sendTo(packet.senderClient, packet.targetClient)
            globals.logger.log(self.client.ip, "received SRCH-Packet. This simulation doesnt handle logins, thus responding with SACK-Packet.")
        elif packet.type == "SACK":
            #ignore - logic not simulated!
            pass
        elif packet.type == "RMVE":
            globals.logger.log(self.client.ip, "received RMVE from", packet.senderClient.ip)
            self.client.receivedPackets.append(packet)
            if not any(p.cId == packet.cId for p in self.client.runningProcesses if p.type == "Update"):
                process.RemoveUpdate(self.client, globals.TIME, packet, False)
            else:
                updateProcessActive = False
                for runningProcess in self.client.runningProcesses:
                        if runningProcess.type == "Update" and runningProcess.cId == packet.cId:
                            runningProcess.receiveAnswer(packet)
                            updateProcessActive = True
                            break
                if not updateProcessActive and not packet.instantReturn:
                    p = copy(packet)
                    p.instantReturn = True
                    p.sendTo(packet.senderClient, self.client)
                    globals.logger.log(self.client.ip, "Instantly Returned Packet", packet.type)
            processes = [p for p in self.client.runningProcesses]
            for p in processes:
                if p.type == "Wait(STOP)" or p.type == "Stop":
                    p.endProcess()
            if len(self.client.actionTimers) > 0:
                self.client.clearTimers(self.client.meshes[0])
        elif packet.type == "UPDT":
            globals.logger.log(self.client.ip, "received UPDT from", packet.senderClient.ip, "and responded with UACK.")
            uack = UACK(packet.cId)
            uack.sendTo(packet.senderClient, packet.targetClient)
            processes = [p for p in self.client.runningProcesses]
            for p in processes:
                if p.type == "UpdateConfirmation" and p.cId == packet.cId:
                    p.receiveUpdt(packet)
                    break
            for p in processes:
                if p.type == "Update" and p.cId == packet.cId:
                    p.endProcess()
        elif packet.type == "UACK":
            globals.logger.log(self.client.ip, "received UACK from", packet.senderClient.ip)
            self.client.receivedPackets.append(packet)
        elif packet.type == "COLL":
            p = self.client.getProcessByType("PingMetrics")
            if p:
                p.receiveColl(packet)
        elif packet.type == "CONN":
            p = self.client.getProcessByType("PingMetrics")
            p.receiveConn(packet)
        elif packet.type == "MESH":
            globals.logger.log(self.client.ip, "received MESH, acknowledging Transmission with MACK for", packet.senderClient.ip)
            ack = MACK(packet.cId)
            ack.sendTo(packet.senderClient, self.client)
            processActive = False
            for p in self.client.runningProcesses:
                if p.type == "MeshRmve" and p.cId == packet.cId:
                    p.receiveMeshAndExit(packet)
                    processActive = True
                    globals.logger.log(self.client.ip, "run receiveMeshAndExit Function")
            if not processActive:
                globals.logger.log(self.client.ip, "had no MeshRmve Process active, saved packet.")
                self.client.receivedPackets.append(packet)
        elif packet.type == "MACK":
            for p in self.client.runningProcesses:
                if p.type == "Wait(MESH)" and p.packet.cId == packet.cId:
                    p.receiveAck(packet)
        elif packet.type == "PING":
            self.client.receivedPackets.append(packet)
            if not packet.alive:
                #Ping Metrics
                p = self.client.getProcessByType("PingMetrics")
                wantResponse = p != None
                if not packet.return_1 and not packet.return_2:
                    #Respond to initial Ping
                    globals.logger.log(self.client.ip, "received initial Ping from", packet.senderClient.ip, "and responded.", packet.timestamp)
                    return_packet = PING(True, False, wantResponse, False, packet.timestamp)
                    return_packet.sendTo(packet.senderClient, self.client)
                    if p:
                        p.receiveInitialPing(packet)
                    else:
                        globals.logger.log("PingProcess was not running on Client!")
                elif packet.return_1 and not packet.return_2:
                    #Respond to Return_1
                    if packet.respond:
                        return_packet = PING(True, True, wantResponse, False, packet.timestamp)
                        return_packet.sendTo(packet.senderClient, self.client)
                        globals.logger.log(self.client.ip, "received Return_1 Ping from", packet.senderClient.ip, "and responded.")
                    else:
                        globals.logger.log(self.client.ip, "received Return_1 Ping from", packet.senderClient.ip)
                    if p:
                        #process might already been closed when last packet arrives
                        p.sendPing(packet.senderClient, packet.timestamp)
                        p.receivePingResponse(packet)
                elif packet.return_2:
                    #Respond to Return_2
                    globals.logger.log(self.client.ip, "received Return_2 Ping from", packet.senderClient.ip)
                    if p:
                        p.receivePingResponse(packet)
        elif packet.type == "STOP":
            globals.logger.log(self.client.ip, "received STOP from", packet.senderClient.ip)
            self.client.receivedPackets.append(packet)
            if not any(True for p in self.client.runningProcesses if p.type == "Stop"):
                if not packet.senderClient == self:
                    self.client.canStartUpdate = False
                p = process.Stop(self.client, globals.TIME)
        elif packet.type == "MISS":
            self.client.receivedPackets.append(packet)
        elif packet.type == "STRX":
            globals.logger.log(self.client.ip, "received STRX from", packet.senderClient.ip)
            neighbours = [c for c in self.client.neighbours if c != packet.senderClient]
            for client in neighbours:
                strx = copy(packet)
                strx.sendTo(client, self.client)
                process.WaitForTXAK(self.client, globals.TIME, strx, 0.1, client)
                globals.logger.log("     Forwarding to", client.ip)
            txak = TXAK(packet.txid)
            txak.sendTo(packet.senderClient, self.client)
        elif packet.type == "TXAK":
            globals.logger.log(self.client.ip, "received TXAK from", packet.senderClient.ip)
            # self.client.receivedPackets.append(packet)
            numOfProcesses = 0
            for p in self.client.runningProcesses:
                if p.type == "Wait(STRX)" and p.targetClient == packet.senderClient:
                    p.receiveAck(packet)
                    numOfProcesses += 1
            globals.logger.log(self.client.ip, "received TXAK and ended", str(numOfProcesses), "processes.")
        elif packet.type == "AudioTx":
            globals.logger.log(self.client.ip, "received 1s of Audio from", packet.senderClient.ip)
            neighbours = [c for c in self.client.neighbours if c != packet.senderClient]
            for client in neighbours:
                audiodata = copy(packet) #might this be a problem with the python garbage collection?
                audiodata.sendTo(client, self.client)
            pass
        elif packet.type == "TACK":
            pass
        elif packet.type == "IDNT":
            pass
        elif packet.type == "AUTH":
            pass
        elif packet.type == "LGIN":
            pass
        elif packet.type == "ADD+":
            pass
        else:
            return TimeoutError()

class PacketTimer:
    def __init__(self, interval, timeroffset, packet, client, toClient):
        self.interval = interval
        self.packet = packet
        self.client = client
        self.toClient = toClient
        self.time = interval - timeroffset
        self.sentPackets = []

    def setRandomTime(self):
        self.time = random.random() + globals.TIME_STEP_SIZE * (self.interval - globals.TIME_STEP_SIZE)

    def tick(self):
        self.time -= globals.TIME_STEP_SIZE
        if self.time <= 0:
            packet = copy(self.packet)
            packet.sendTo(self.toClient, self.client)
            self.sentPackets.append(copy(packet))
            self.resetTimer()

    def resetTimer(self):
        self.time = self.interval

class ActionTimer:
    def __init__(self, interval, timeroffset, action, forClient, selfClient):
        self.interval = interval
        self.time = interval - timeroffset
        self.action = action
        self.forClient = forClient
        self.selfClient = selfClient
        self.playing = True
    
    def resetTimer(self):
        self.time = self.interval

    def tick(self):
        if self.playing:
            self.time -= globals.TIME_STEP_SIZE
            if self.time <= 0:
                self.resetTimer()
                return True
            return False
        return False

    def pause(self):
        self.playing = False

    def play(self):
        self.playing = True