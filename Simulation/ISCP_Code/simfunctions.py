#pylint: disable=logging-too-many-args
#pylint: disable=unused-wildcard-import
import random
from packets import SRCH
import globals

instances = []
def prepareSimFunctions(clients):
    instances.append(SeachPacketSimulation(clients))

def runSimFunctions(time):
    for instance in instances:
        instance.timeUpdate(time)

class SeachPacketSimulation:
    def __init__(self, clients):
        self.interval = 2
        self.nextSearchTime = random.uniform(0.1, self.interval)
        self.clients = clients

    def getRandomClient(self):
        while True:
            senderClient = random.choice(self.clients)
            if senderClient.alive:
                break
        return senderClient

    def timeUpdate(self, time):
        if time >= self.nextSearchTime:
            self.nextSearchTime += self.interval
            senderClient = self.getRandomClient()
            targetClient = self.getRandomClient()
            srch = SRCH()
            srch.sendTo(targetClient, senderClient)
            globals.logger.log(senderClient.ip, "sent SRCH to", targetClient.ip)