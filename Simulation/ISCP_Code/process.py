#pylint: disable=logging-too-many-args
#pylint: disable=unused-wildcard-import
from packets import *
from copy import copy, deepcopy
import random
from mesh import Mesh
import sys

class Process:
    def __init__(self, client, starttime):
        self.client = client
        self.starttime = starttime
        self.running = True
        self.client.runningProcesses.append(self)
        self.activeFunctions = []

    def timeUpdate(self):
        for f in self.activeFunctions:
            f()

    def endProcess(self):
        functions = [func for func in self.activeFunctions]
        for f in functions:
            self.activeFunctions.remove(f)
        self.client.runningProcesses.remove(self)
        self.running = False
        self.client.oldProcesses.append(self)
        self.endtime = globals.TIME

class Stop(Process):
    #If received Stop Packet send ALVE to all Neighbours and wait 500ms. Then send MISS packets to Initiator.
    def __init__(self, client, starttime):
        super().__init__(client, starttime)
        self.type = "Stop"
        for toClient in client.neighbours:
                alve = ALVE()
                alve.sendTo(toClient, client)
        self.waitForAliveUntil = globals.TIME + 0.2
        self.activeFunctions.append(self.waitForAlive)
    
    def waitForAlive(self):
        if globals.TIME >= self.waitForAliveUntil:
            availableClients = []
            if self.client.ip == "192.168.0.46" or self.client.ip == "192.168.0.48" or self.client.ip == "192.168.0.50":
                print("Stop")
            for packet in self.client.receivedPackets:
                if packet.type == "ALVE" and packet.receiveTime > globals.TIME - 3 and packet.senderClient not in availableClients:
                    availableClients.append(packet.senderClient)
            missingClients = [x for x in self.client.neighbours if x not in availableClients]
            if len(missingClients) > 0:
                globals.logger.log(self.client.ip, "sending 3x MISS with", len(missingClients), "missing Clients.")
                packets = [MISS(missingClients), MISS(missingClients), MISS(missingClients)]
                initiatorClient = self.getInitiatorClient()
                for packet in packets:
                    packet.sendTo(initiatorClient, self.client)
            else:
                globals.logger.log(self.client.ip, "didnt detect any missing members and thus wont send MISS.")
            self.endProcess()

    def getInitiatorClient(self):
        stop_senders = [packet.senderClient for packet in self.client.receivedPackets if packet.type == "STOP"]
        initiator = stop_senders[0]
        for client in stop_senders:
            if ipSmaller(client.ip, initiator.ip):
                initiator = client
        return initiator

    def endProcess(self):
        super().endProcess()
        packets = [packet for packet in self.client.receivedPackets if packet.type == "STOP"]
        waitProcessRunning = any(True for p in self.client.runningProcesses if p.type == "Wait(STOP)")
        if not waitProcessRunning:
            for packet in packets:
                if packet.type == "STOP":
                    self.client.receivedPackets.remove(packet)

class WaitForMissAndStop(Process):
    def __init__(self, client, starttime):
        super().__init__(client, starttime)
        self.type = "Wait(STOP)"
        self.client.pauseTimers()
        self.waitForStopsUntil = globals.TIME + 1
        self.activeFunctions.append(self.waitForStops)

    def waitForStops(self):
        if globals.TIME >= self.waitForStopsUntil:
            isUpdateInitiator = not any(packet.type == "STOP" and ipSmaller(packet.senderClient.ip, self.client.ip) for packet in self.client.receivedPackets)
            if isUpdateInitiator:
                missingClients = []
                for packet in self.client.receivedPackets:
                    if packet.type == "MISS":
                        for ms in packet.missingClients:
                            if ms not in missingClients:
                                missingClients.append(ms)
                self.startUpdate(missingClients)
            self.endProcess()

    def startUpdate(self, missingClients):
        if missingClients:
            self.client.clearTimers(self.client.meshes[0])
            cId = random.randint(0, 65535)
            globals.logger.log(self.client.ip, "Initiating Remove-Update Round " + str(cId) + ". Missing Clients:")
            for client in missingClients:
                globals.logger.log("     ", client.ip)
            packet = RMVE(missingClients, cId, False)
            RemoveUpdate(self.client, globals.TIME, packet, True)
        else:
            globals.logger.log(self.client.ip, "didnt start new update as no clients were missing!")
            self.client.playTimers()

    def endProcess(self):
        super().endProcess()
        packets = [packet for packet in self.client.receivedPackets]
        otherStopProcessRunning = any(True for p in self.client.runningProcesses if p.type == "Stop")
        otherWaitProcessRunning = any(True for p in self.client.runningProcesses if p.type == "Wait(STOP)")
        for packet in packets:
            if packet.type == "STOP" and not otherStopProcessRunning:
                self.client.receivedPackets.remove(packet)
            elif packet.type == "MISS" and not otherWaitProcessRunning:
                self.client.receivedPackets.remove(packet)

class WaitForAck(Process):
    def __init__(self, client, starttime, packet_to_resend, timeoutInterval, targetClient):
        super().__init__(client, starttime)
        self.type = "Wait(" + packet_to_resend.type + ")"
        self.waitForAckUntil = globals.TIME + timeoutInterval
        self.timeoutInterval = timeoutInterval
        self.packet = packet_to_resend
        self.targetClient = targetClient
        self.activeFunctions.append(self.waitForAck)

    def waitForAck(self):
        if globals.TIME >= self.waitForAckUntil:
            if self.ackArrived():
                globals.logger.log(self.client.ip, "ACK for", self.packet.type, "arrived from", self.targetClient.ip)
                self.endProcess()
            else:
                globals.logger.log(self.client.ip, "resends", self.packet.type, "as no ACK was received from", self.targetClient.ip)
                self.waitForAckUntil = globals.TIME + self.timeoutInterval
                self.resendPacket()

    def ackArrived(self):
        for packet in self.client.receivedPackets:
            if self.isRightPacket(packet):
                return True
        return False

    def isRightPacket(self, packet):
        #Implement in Subclass
        pass

    def resendPacket(self):
        globals.logger.log(self.client.ip, "retransmitted", self.packet.type, "to", self.targetClient.ip, "as no ACK was received in time.")
        packet = copy(self.packet)
        packet.sendTo(self.targetClient, self.client)

    def receiveAck(self, packet):
        if self.isRightPacket(packet):
            self.endProcess()

    def endProcess(self):
        super().endProcess()
        packets = [packet for packet in self.client.receivedPackets]
        for packet in packets:
            if self.isRightPacket(packet):
                self.client.receivedPackets.remove(packet)
        globals.logger.log(self.client.ip, "finished Waiting for", self.packet.type, "Acknowledgement from", self.targetClient.ip)

class WaitForUAck(WaitForAck):
    def isRightPacket(self, packet):
        return packet.type == "UACK" and packet.cId == self.packet.cId and packet.senderClient == self.targetClient

class WaitForMAck(WaitForAck):
    def isRightPacket(self, packet):
        return packet.type == "MACK" and packet.cId == self.packet.cId and packet.senderClient == self.targetClient

class WaitForTXAK(WaitForAck):
    def isRightPacket(self, packet):
        return packet.type == "TXAK" and packet.txid == self.packet.txid and packet.senderClient == self.targetClient

class Update(Process):
    def __init__(self, client, starttime, packet, isInitiator):
        if not self.updateProcessAlreadyRunning(client, packet.cId):
            super().__init__(client, starttime)
            self.cId = packet.cId
            self.packetType = packet.type
            self.packet = packet
            self.isInitiator = isInitiator
            self.type = "Update"
            globals.logger.log(self.client.ip, "started Update Process.")
            self.parent = None if self.isInitiator else packet.senderClient
            self.missingResponsesFrom = []
            self.livingNeighbours = []
            self.sentPackets = []
            self.receiveAnswerFromAllNeighbours = False
            self.heartbeatInterval = 0.065
            self.relevantNeighbours = self.getRelevantNeighbours()
            self.forwardPacketToAllNeighbours()
            self.startSendingHeartbeats()
        else:
            globals.logger.log(client.ip, "didnt start Update Process as another instance was already running.")

    def updateProcessAlreadyRunning(self, client, cId):
        #implement in subclass!
        return []

    def forwardPacketToAllNeighbours(self):
        if len(self.relevantNeighbours) == 0 and not self.isInitiator:
            packet = copy(self.packet)
            packet.sendTo(self.parent, self.client)
            globals.logger.log(self.client.ip, "started WaitToEnableUpdate.")
            WaitToEnableUpdate(self.client, globals.TIME, self.packet.clientlist, self.cId, self.livingNeighbours)
            self.endProcess()
            globals.logger.log(self.client.ip, "detected as Leave-Member: Send back packet")
        else:
            for client in self.relevantNeighbours:
                if client != self.parent:
                    self.sendPacketAndListClient(client)
            self.activeFunctions.append(self.waitForHearbeats)
            self.nextHeartbeatCheck = globals.TIME + self.heartbeatInterval

    def sendPacketAndListClient(self, client):
        globals.logger.log(self.client.ip, "Forwarded", self.packetType, "to", client.ip)
        packet = copy(self.packet)
        packet.sendTo(client, self.client)
        self.missingResponsesFrom.append(client)
        self.livingNeighbours.append(client)
        self.sentPackets.append(packet)
    
    def waitForHearbeats(self):
        if self.nextHeartbeatCheck <= globals.TIME:
            #Two Heartbeats missing: Packet loss assumed
            #Eight Heartbeats missing: Client died -> Cancel Update
            missingResponses = copy(self.missingResponsesFrom)
            for client in missingResponses: 
                missingTwoHeartbeats = not any(True for packet in self.client.receivedPackets if packet.type == "PING" and packet.alive and packet.senderClient == client and packet.receiveTime > globals.TIME - 3 * self.heartbeatInterval)
                missingEightHeartbeats = not any(True for packet in self.client.receivedPackets if packet.type == "PING" and packet.alive and packet.senderClient == client and packet.receiveTime > globals.TIME - 9 * self.heartbeatInterval)
                receivedPingResponseFromClient = any(True for packet in self.client.receivedPackets if packet.type == "PING" and packet.alive and packet.senderClient == client)
                if not self.sentPacketDuringLastInterval(client):
                    if missingEightHeartbeats and receivedPingResponseFromClient:
                        globals.logger.log(self.client.ip, "lost connection to", client.ip, "during Update Process.")
                        self.endProcess()
                    elif missingTwoHeartbeats: #case abfangen noch kein ping empfangen!
                            globals.logger.log(self.client.ip, "assumed Packetloss of RMVE to", client.ip, "and retransmits packet.")
                            packet = copy(self.packet)
                            packet.sendTo(client, self.client)
                            self.sentPackets.append(packet)
                        
    def sentPacketDuringLastInterval(self, client):
        for packet in self.sentPackets:
            if packet.targetClient == client and packet.sentTime > globals.TIME - 2 * self.heartbeatInterval:
                return True
        return False

    def receiveAnswer(self, packet):
        if not self.receiveAnswerFromAllNeighbours:
            globals.logger.log(self.client.ip, "Received", self.packetType + "-Answer from", packet.senderClient.ip)
            if packet.senderClient in self.missingResponsesFrom:
                self.missingResponsesFrom.remove(packet.senderClient)
            else:
                globals.logger.log(self.client.ip, "received unwanted RMVE from:", packet.senderClient.ip, "If this was his parent(" +  self.parent.ip + ") everything is ok, some heartbeats just got lost due to packetloss and the RMVE has been retransmitted.")
                ping = PING(False, False, False, True, 0)
                ping.sendTo(packet.senderClient, self.client)
                if packet.senderClient != self.parent:
                    print("Uh Oh, this is a problem, isnt it?")
            if len(self.missingResponsesFrom) == 0:
                globals.logger.log(self.client.ip, "received Responses from all Neighbours and startet WaitToEnableUpdate.")
                confirmation_process = WaitToEnableUpdate(self.client, globals.TIME, self.packet.clientlist, self.cId, self.livingNeighbours)
                if self.isInitiator:
                    if self.packetType == "RMVE" or self.packetType == "ADD+":
                        globals.logger.log(self.client.ip + ": Update", self.cId, "successfull. Start confirming it.")
                        confirmation_process.startUpdate()
                        self.endProcess()
                else:
                    p1 = copy(packet)
                    p1.sendTo(self.parent, self.client)
                    self.receiveAnswerFromAllNeighbours = True
                    self.activeFunctions.remove(self.sendHeartbeats)
        else:
            globals.logger.log(self.client.ip, "already received responses from all Neighbours. Maybe the sent back RMVE got lost? Transmitting another RMVE...")
            p1 = copy(packet)
            p1.sendTo(packet.senderClient, self.client)

    def startSendingHeartbeats(self):
        self.nextHeartbeat = globals.TIME
        self.timestamp = 0
        self.sendHeartbeats()
        self.activeFunctions.append(self.sendHeartbeats)

    def sendHeartbeats(self):
        if self.nextHeartbeat <= globals.TIME and not self.isInitiator:
            heartbeat = PING(False, False, False, True, self.timestamp)
            heartbeat.sendTo(self.parent, self.client)
            self.timestamp += 1
            self.nextHeartbeat = globals.TIME + self.heartbeatInterval

    def endProcess(self):
        packets = [packet for packet in self.client.receivedPackets]
        for packet in packets:
            if packet.type == "RMVE" and packet.cId == self.cId:
                self.client.receivedPackets.remove(packet)
        packets = [packet for packet in self.client.receivedPackets]
        for packet in packets:
            if packet.type == "PING" and packet.alive: #this wont work for two simultanious Updates but that should never be the case!
                self.client.receivedPackets.remove(packet)
        super().endProcess()

    def getRelevantNeighbours(self):
        return []

class RemoveUpdate(Update):
    def updateProcessAlreadyRunning(self, client, cId):
        for process in client.runningProcesses:
            if process.type == "CreateMesh" and process.cId == cId:
                return True
        return False

    def getRelevantNeighbours(self):
        missingClients = self.packet.clientlist
        clients_to_ignore = [self.parent, self.client]
        relevantNeighbours = [client for client in self.client.neighbours if client not in missingClients and client not in clients_to_ignore]
        self.getRelevantSubclients(self.client, relevantNeighbours, missingClients, clients_to_ignore)
        return relevantNeighbours
            
    def getRelevantSubclients(self, client, relevantNeighbours, missingClients, clients_to_ignore):
        for neighbour in client.neighbours:
            if neighbour not in clients_to_ignore:
                if neighbour not in relevantNeighbours and neighbour not in missingClients:
                    relevantNeighbours.append(neighbour)
                elif neighbour in missingClients:
                    clients_to_ignore.append(neighbour)
                    self.getRelevantSubclients(neighbour, relevantNeighbours, missingClients, clients_to_ignore)

class AddUpdate(Update):
    def getRelevantNeighbours(self):
        return [client for client in self.client.neighbours if client != self.parent]

class WaitToEnableUpdate(Process):
    def __init__(self, client, starttime, missingClients, cId, neighbours):
        if not self.waitProcessAlreadyRunning(client, cId):
            super().__init__(client, starttime)
            self.missingClients = missingClients
            self.cId = cId
            self.type = "UpdateConfirmation"
            self.alreadyReceivedUpdt = False
            self.neighbours = neighbours #relevant neighbours from update round, including those from missing neighbours
            self.waitForUpdtUntil = globals.TIME + 4 #4s should be enough for a lot of Clients (simulated with 100 at 5% packetloss)
            self.activeFunctions.append(self.deleteChangesIfNoAck)
        else:
            globals.logger.log(client.ip, "didnt spawn new WaitToEnableUpdateProcess as there was already one running.")

    def waitProcessAlreadyRunning(self, client, cId):
        for process in client.runningProcesses:
            if process.type == "UpdateConfirmation" and process.cId == cId:
                return True
        return False

    def deleteChangesIfNoAck(self):
        if globals.TIME >= self.waitForUpdtUntil:
            self.endProcess()
            globals.logger.log(self.client.ip, "ERROR: Didnt receive UPDT in time, discarding changes.")

    def receiveUpdt(self, packet):
        if not self.alreadyReceivedUpdt:
            self.alreadyReceivedUpdt = True
            if packet.senderClient in self.neighbours:
                self.neighbours.remove(packet.senderClient)
            self.sendUpdtToNeighbours()
            self.applyUpdate()

    def sendUpdtToNeighbours(self):
        for client in self.neighbours:
            globals.logger.log(self.client.ip, "send UPDT to", client.ip + ". Waiting for UACK...")
            packet = UPDT(self.cId)
            packet.sendTo(client, self.client)
            WaitForUAck(self.client, globals.TIME, packet, 0.2, client)

    def applyUpdate(self):
        #Start Mesh Creation
        globals.logger.log(self.client.ip, "applied Update", self.cId, ". I should start Creating a Mesh now!")
        self.endProcess()
        globals.logger.log(self.client.ip, "starting CreateMesh_Remove with missing Clients:")
        for client in self.missingClients:
            globals.logger.log("     ", client.ip)
        CreateMesh_Remove(self.client, globals.TIME, self.missingClients, self.cId, self.client.meshes[0])

    def startUpdate(self):
        self.sendUpdtToNeighbours()
        self.applyUpdate()

class CreateMesh(Process):
    def __init__(self, client, starttime, cId, mesh):
        if not self.meshProcessAlreadyRunning(client, cId):
            super().__init__(client, starttime)
            self.cId = cId
            self.tempCId = cId
            self.mesh = mesh.duplicate()
            self.oldMesh = mesh.duplicate()
            self.type = "CreateMesh"
            self.timeoutInterval = 2
            self.receivedMesh = False
            self.waitForMackUntil = None
            self.waitForMeshUntil = None
            for process in self.client.runningProcesses:
                if process != self and process.type == self.type and process.cId == self.cId:
                    print("This shouldnt happen!")
            if self.NewMeshAlreadyArrived():
                globals.logger.log(self.client.ip, "already received MESH before Porcess started. Continue as received MESH regularly.")
                packet = self.getMESHFromReceivedPackets()
                self.client.receivedPackets.remove(packet)
                self.receiveMeshAndExit(packet)
            else:
                self.updateMesh()
        else:
            globals.logger.log(client.ip, "didnt start CreateMesh Process as another instance is already running.")

    def meshProcessAlreadyRunning(self, client, cId):
        for process in client.runningProcesses:
            if process.type == "CreateMesh" and process.cId == cId:
                return True
        return False

    def updateMesh(self):
        pass

    def NewMeshAlreadyArrived(self):
        return self.getMESHFromReceivedPackets() != None

    def getMESHFromReceivedPackets(self):
        for packet in self.client.receivedPackets:
            if packet.type == "MESH" and packet.cId == self.cId:
                return packet
        return

    @property
    def isTemporaryMaster(self):
        return self.tempCId % len(self.mesh.clients) == self.mesh.getClientId(self.client)

    def applyMesh(self, mesh):
        #only for mesh[0]:
        self.mesh = mesh.duplicate()
        self.client.meshes[0] = mesh.duplicate()
        globals.logger.log(self.client.ip, "applied Mesh and connected to Neighbours.")
        for client in self.mesh.getNeighbours(self.client):
            client.connect(self.client)
            globals.logger.log("     connected to", client.ip)
        self.client.canStartUpdate = True #This should be done after all ACKs are received!

    def distributeMesh(self):
        globals.logger.log(self.client.ip, "starts distributing MESH.")
        packet = MESH(self.mesh, self.cId, 0)
        packet.senderClient = self.client
        self.forwardMesh(packet)

    def receiveMeshAndExit(self, packet):
        if not self.receivedMesh:
            if self.waitForMesh in self.activeFunctions:
                self.activeFunctions.remove(self.waitForMesh)
            self.applyMesh(packet.mesh)
            self.receivedMesh = True
            self.forwardMesh(packet)
        else:
            globals.logger.log(self.client.ip, "already received Mesh. No further Actions.")
        self.endProcess()

    def forwardMesh(self, packet):
        neighbours = [client for client in self.mesh.getNeighbours(self.client) if client != packet.senderClient]
        if len(neighbours) == 0:
            globals.logger.log(self.client.ip, "is a leave Member and cant forward the MESH anymore. Exiting MeshRmve Process.")
        else:
            for client in neighbours:
                p = copy(packet)
                p.sendTo(client, self.client)
                WaitForMAck(self.client, globals.TIME, p, 0.2, client)
                globals.logger.log(self.client.ip, "forwarded MESH to", client.ip)

    def waitForMesh(self):
        if globals.TIME >= self.waitForMeshUntil + self.timeoutInterval:
            globals.logger.log(self.client.ip, "ERROR: Didnt receive MESH in Time. Electing new Master.")
            self.tempCId += 1
            self.waitForMeshUntil += self.timeoutInterval

    def endProcess(self):
        super().endProcess()

class CreateMesh_Remove(CreateMesh):
    def __init__(self, client, starttime, missingClients, cId, mesh):
        if not self.meshProcessAlreadyRunning(client, cId):
            self.missingClients = missingClients
            super().__init__(client, starttime, cId, mesh)
            self.type = "MeshRmve"
        else:
            globals.logger.log(self.client.ip, "didnt open a new CreateMesh_Remove Process as there was already one running.")

    def meshProcessAlreadyRunning(self, client, cId):
        for process in client.runningProcesses:
            if process.type == "CreateMesh" and process.cId == cId:
                return True
        return False

    def updateMesh(self):
        self.removeMissingMembers() #This is needed so the modulo election works
        if self.onlyLeaveMembersMissing:
            if self.isTemporaryMaster:
                globals.logger.log(self.client.ip, "elected temporary Master.")
                self.activeFunctions.append(self.waitToDistributeMesh)
                self.distributeAt = globals.TIME + 0.1
            else:
                self.waitForMeshUntil = globals.TIME + self.timeoutInterval
                self.activeFunctions.append(self.waitForMesh)
                globals.logger.log(self.client.ip, "waiting for updated MESH.")
        else:
            self.gatherPingMetrics()

    @property
    def onlyLeaveMembersMissing(self):
        for client in self.missingClients:
            if client.isCoreMember(self.oldMesh):
                return False
        return True

    def removeMissingMembers(self):
        for client in self.missingClients:
            if client in self.mesh.clients:
                self.mesh.removeClient(client)

    def waitToDistributeMesh(self):
        if globals.TIME >= self.distributeAt:
            self.applyMesh(self.mesh)
            self.distributeMesh()
            if self.waitToDistributeMesh in self.activeFunctions:
                self.activeFunctions.remove(self.waitToDistributeMesh)

    def gatherPingMetrics(self):
        GetPingMetrics(self.client, globals.TIME, self.mesh, self.isTemporaryMaster)
        self.activeFunctions.append(self.waitForMesh)
        self.waitForMeshUntil = globals.TIME + self.timeoutInterval * 2

    def takeoverMetricsAndGenerateMesh(self, metrics):
        self.metrics = metrics
        self.activeFunctions = []
        self.calculateMesh()
        self.distributeMesh()
        self.endProcess()

    def calculateMesh(self):
        self.sortMetricsAscendingOrder()
        self.addMissingMetricsAndRemoveDoubles()
        self.testMetrics()
        newMesh = Mesh(0)
        while len(newMesh.connections) < len(self.mesh.clients)-1:
            #if there are n connection, n+1 clients are connected!
            if len(self.metrics) == 0:
                globals.logger.log("Mesh Creation Error: Not Enough Connections to connect all Clients!")
            if not newMesh.connectionWouldCreateLoop(self.metrics[0].fromClient, self.metrics[0].toClient):
                fromClient = self.metrics[0].fromClient
                toClient = self.metrics[0].toClient
                newMesh.addConnection(fromClient, toClient)
            self.metrics.remove(self.metrics[0])
        globals.logger.log(self.client.ip, "finished Mesh calculation.")
        self.applyMesh(newMesh)
        self.testMesh()
        newMesh.writeFile()

    def testMesh(self):
        clientsMissingInMesh = [client for client in globals.clients if client.alive and client not in self.mesh.clients]
        clientsThatShouldNotBeInMesh = [client for client in globals.clients if not client.alive and client in self.mesh.clients]
        if clientsMissingInMesh:
            print("CLIENTS ARE MISSING!!!")
        else:
            print("ALL ALIVE CLIENTS IN MESH!")
        if clientsThatShouldNotBeInMesh:
            print("DEAD CLIENTS ARE IN MESH!!!")
        else:
            print("NO DEAD CLIENTS IN MESH.")

    def testMetrics(self):
        allClients = [client for client in self.mesh.clients]
        metricsClients = []
        for entry in self.metrics:
            for client in [entry.fromClient, entry.toClient]:
                if client not in metricsClients:
                    metricsClients.append(client)
        copyAll = [client for client in allClients]
        for client in copyAll:
            if client in metricsClients:
                allClients.remove(client)
        print("Testint Metrics. Missing Clients:")
        for client in self.missingClients:
            print("     ", client.ip)
        for entry in self.metrics:
            if entry.fromClient in self.missingClients or entry.toClient in self.missingClients:
                print("Test failed!")
        if len(allClients) > 0:
            print("Metrics not complete.")
        else:
            print("Metrics OK")

    def sortMetricsAscendingOrder(self):
        metrics = sorted(self.metrics, key=lambda entry: entry.latency)
        self.metrics = metrics

    def addMissingMetricsAndRemoveDoubles(self):
        for client in self.mesh.clients:
            metricsContainClient = any(True for entry in self.metrics if entry.fromClient == client or entry.toClient == client)
            if not metricsContainClient:
                globals.logger.log("Found missing metric for client", client.ip, "adding one...")
                worseLatency = self.metrics[len(self.metrics) - 1].latency + 0.002
                randomClient = self.mesh.clients[random.randint(0, len(self.mesh.clients) - 1)]
                self.metrics.append(LatencyEntry(client, randomClient, worseLatency))
        metrics = [metric for metric in self.metrics]
        for connection in metrics:
            if connection.fromClient in self.missingClients or connection.toClient in self.missingClients:
                    self.metrics.remove(connection)
                    break
        metrics = [metric for metric in self.metrics]
        for connection in metrics:
            for conn in metrics:
                if connection != conn and connection.fromClient == conn.fromClient and connection.toClient == conn.toClient:
                    connection.latency = (connection.latency + conn.latency) / 2
                    self.metrics.remove(conn)

    def endProcess(self):
        super().endProcess()
        packets = [packet for packet in self.client.receivedPackets]
        for packet in packets:
            if packet.type == "RMVE" and packet.cId == self.cId:
                self.client.receivedPackets.remove(packet)

class GetPingMetrics(Process):
    def __init__(self, client, starttime, mesh, isInitiator=False):
        if not self.pingProcessAlreadyRunning(client, mesh):
            super().__init__(client, starttime)
            self.type = "PingMetrics"
            self.timeoutInterval = 1
            self.mesh = mesh.duplicate() #mesh should only contain clients that are alive!
            self.clientsToPing = [client for client in mesh.clients if client != self.client]
            self.metrics = []
            self.isInitiator = isInitiator
            random.shuffle(self.clientsToPing)
            self.nextPingTime = globals.TIME
            self.sentPings = []
            self.activeFunctions.append(self.pingNextClient)
            self.activeFunctions.append(self.timeOut)
            self.timeoutAt = globals.TIME + self.timeoutInterval
            self.inFinishedState = False
            self.requestedConns = False
        else:
            globals.logger.log(client.ip, "didnt open a new PingMetrics Process as there was already one running.")

    def pingProcessAlreadyRunning(self, client, mesh):
        for process in client.runningProcesses:
            if process.type == "PingMetrics" and process.mesh.groupId == mesh.groupId:
                return True
        return False

    def pingNextClient(self):
        if self.starttime + self.timeoutInterval * 4 > globals.TIME:
            if self.nextPingTime <= globals.TIME:
                self.nextPingTime = self.getRandomPingTime()
                client = self.getRandomClientWithNoMetric()
                if client:
                    ping = PING(False, False, True, False, random.randint(0, 65535))
                    ping.sendTo(client, self.client)
                    globals.logger.log(self.client.ip, "sent initial ping to", client.ip, "with timestamp", ping.timestamp)
                    self.sendPing(client, ping.timestamp)
        else:
            print("I should never end up here.", self.waitForCollUntil, globals.TIME)
            self.endProcess()

    def getRandomPingTime(self):
        rnd = random.uniform(0.01, 0.02)
        return globals.TIME + rnd

    def getRandomClientWithNoMetric(self):
        estimatedRoundtrip = 0.02
        possibleClients = [client for client in self.clientsToPing]
        clientsWithMetrics = [entry.toClient for entry in self.metrics]
        for client in self.clientsToPing:
            if client in clientsWithMetrics:
                possibleClients.remove(client)
        for entry in self.sentPings:
            if entry.toClient in possibleClients and entry.timestamp > globals.TIME - estimatedRoundtrip * 2:
                possibleClients.remove(entry.toClient)
        if len(possibleClients) > 0:
            return random.choice(possibleClients)
        return False

    def timeOut(self):
        if globals.TIME >= self.timeoutAt and not self.requestedConns:
            if self.isInitiator:
                globals.logger.log(self.client.ip, "Timeout: Aborting Ping Measurement and requesting Data from other clients.")
                self.activeFunctions.remove(self.pingNextClient)
                self.requestConns()
            else:
                globals.logger.log(self.client.ip, "Timeout: Sending no more pings and waiting for COLLs.")
                self.waitForCollUntil = globals.TIME + self.timeoutInterval
                self.activeFunctions = [self.waitForColl]
                #End Process here if no Coll recceived

    def receiveInitialPing(self, packet):
        self.sendPing(packet.senderClient, packet.timestamp)

    def sendPing(self, toClient, timestamp):
        #Pseudofuction, the real Response gets handled in the Client Class, as it might need to respond without this process running.
        self.sentPings.append(SentPing(toClient, self.client, timestamp))

    def sentPingToClient(self, client):
        for entry in self.sentPings:
            if entry.toClient == client:
                return True
        return False

    def receivePingResponse(self, packet):
        if not self.inFinishedState:
            client = packet.senderClient
            if self.sentPingToClient(client):
                if client in self.clientsToPing:
                    self.clientsToPing.remove(client)
                    roundtrip_latency = globals.TIME - self.getPingSendTime(client, packet.timestamp)
                    self.metrics.append(LatencyEntry(client, self.client, roundtrip_latency))
                    globals.logger.log(self.client.ip, "saves metrics to", client.ip, "with a weight of", roundtrip_latency)
            else:
                globals.logger.log(self.client.ip, "received unwanted or invalid Ping-Response from", packet.senderClient.ip)
            if self.metricsFromAllNeighboursArrived():
                if self.pingNextClient in self.activeFunctions:
                    self.activeFunctions.remove(self.pingNextClient)
                self.inFinishedState = True
                if self.isInitiator:
                    globals.logger.log(self.client.ip, "gathered all Pings. Starting to send Colls.")
                    self.requestConns()
                else:
                    globals.logger.log(self.client.ip, "gathered all Pings. Waiting for Coll-Request.")
                    self.activeFunctions = [self.waitForColl]
                    self.waitForCollUntil = globals.TIME + 4
        else:
            globals.logger.log(self.client.ip, "received Ping Packet but already in finished state.")

    def metricsFromAllNeighboursArrived(self):
        neighbours = [client for client in self.mesh.clients if client != self.client]
        for entry in self.metrics:
            for client in [entry.fromClient, entry.toClient]:
                if client in neighbours:
                    neighbours.remove(client)
        return len(neighbours) == 0

    def requestConns(self):
        clients = [client for client in self.mesh.clients if client != self.client]
        self.pendingClients = []
        for client in clients:
            self.pendingClients.append(client)
            coll = COLL()
            coll.sendTo(client, self.client)
            globals.logger.log(self.client.ip, "sent COLL to", client.ip)
        self.activeFunctions.append(self.waitForConns)
        self.waitForConnsUntil = globals.TIME + self.timeoutInterval
        self.requestedConns = True

    def waitForColl(self):
        if globals.TIME >= self.waitForCollUntil:
            globals.logger.log(self.client.ip, "didnt receive a Coll in time. Discarding Metrics.")
            self.endProcess()

    def receiveColl(self, packet):
        conn = CONN(self.metrics)
        conn.sendTo(packet.senderClient, self.client)
        globals.logger.log(self.client.ip, "sending CONN to", packet.senderClient.ip, "and exiting ping measurement process.")
        self.endProcess()

    def waitForConns(self):
        if globals.TIME >= self.waitForConnsUntil:
            globals.logger.log(self.client.ip, "Conns didnt arrive in Time, handing over existing data and exiting Ping measuring process.")
            self.handoverDataAndExit()

    def receiveConn(self, packet):
        if packet.senderClient in self.pendingClients:
            globals.logger.log(self.client.ip, "received Ping Metrics from", packet.senderClient.ip)
            for entry in packet.metrics:
                if entry not in self.metrics:
                    self.metrics.append(entry)
            self.pendingClients.remove(packet.senderClient)
        if len(self.pendingClients) == 0:
            globals.logger.log(self.client.ip, "received all ping metrics.")
            self.handoverDataAndExit()

    def handoverDataAndExit(self):
        p = self.client.getProcessByType("MeshRmve")
        p.takeoverMetricsAndGenerateMesh(self.metrics)
        self.endProcess()

    def getPingSendTime(self, client, timestamp):
        for ping in self.sentPings:
            if ping.toClient == client and ping.timestamp == timestamp:
                return ping.sendTime
        return Exception("No Ping in sentPings")

    def endProcess(self):
        super().endProcess()
        packets = [packet for packet in self.client.receivedPackets]
        for packet in packets:
            if packet.type == "PING" and not packet.alive:
                self.client.receivedPackets.remove(packet)
        globals.logger.log(self.client.ip, "ended ping measuring process.")

class AudioTransmission(Process):
    def __init__(self, client, targetClients, starttime, length):
        super().__init__(client, starttime)
        self.type = "AudioTransmission"
        self.transmittAudioUntil = starttime + length
        self.sendNextAudioPacketAt = starttime
        self.targetClients = targetClients
        self.activeFunctions.append(self.transmittAudio)

    def transmittAudio(self):
        if globals.TIME >= self.transmittAudioUntil:
            globals.logger.log(self.client.ip, "finished Sending Audio.")
            self.endProcess()
        elif globals.TIME >= self.sendNextAudioPacketAt:
            self.sendNextAudioPacketAt += 1
            for client in self.targetClients:
                audiodata = AudioData()
                audiodata.sendTo(client, self.client)

def ipSmaller(ip1, ip2):
    #only checks the last quarter! Only works for /24 Nets!
    return int(ip1.split(".")[3]) < int(ip2.split(".")[3])

class SentPing:
    def __init__(self, toClient, fromClient, timestamp):
        self.toClient = toClient
        self.fromClient = fromClient
        self.timestamp = timestamp
        self.sendTime = globals.TIME

class LatencyEntry:
    def __init__(self, toClient, fromClient, latency):
        self.toClient = toClient
        self.fromClient = fromClient
        self.latency = latency