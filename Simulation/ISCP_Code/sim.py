#pylint: disable=logging-too-many-args
#pylint: disable=unused-wildcard-import
from client import Client
from mesh import Mesh
import globals
import random
from addons import printTime, printTimeSteps
from simfunctions import *
from timeline import Timeline
import math

#initiate Clients and Mesh Simulation
clients = globals.clients
mesh = Mesh(0)
for i in range(globals.NUM_OF_CLIENTS):
    clients.append(Client("192.168.0." + str(i), mesh))
    if len(clients) > 1:
        alveoffset = (random.random() + globals.TIME_STEP_SIZE) * (2 - globals.TIME_STEP_SIZE)
        mesh.addConnection(clients[i], clients[(i-1)], alveoffset)
mesh.connectMesh()
# clients[len(clients) - 2].die()
clients[len(clients) - 2].die()

#initiate Simulation Events
timeline = Timeline()
#simulation starts with 30s of mesh creation after a single client failed.
timeline.addAction(globals.logger.log, 30, 1, "Starting 10s single Audio Transmission at 30s")
timeline.addAction(clients[2].simulateAudioTransmission, 30, 10, 0, 10)
# timeline.addAction(globals.logger.log, 60, 1, "Starting Overlapping Dual 10s Audio Transmission at 60s")
# timeline.addAction(clients[2].simulateAudioTransmission, 60, 10, 0, 10)
# timeline.addAction(clients[1].simulateAudioTransmission, 65, 10, 0, 10)
# num_of_sending_clients = math.ceil(globals.NUM_OF_CLIENTS / 20)
# timeline.addAction(globals.logger.log, 90, 1, "Starting 10s Audio Transmission from 5% of clients (" + str(num_of_sending_clients) +" Clients) at 90s")
# for i in range(num_of_sending_clients):
#     timeline.addAction(clients[i].simulateAudioTransmission, 90 + num_of_sending_clients / 100, 10, 0, 10)
# timeline.addAction(globals.logger.log, 60, 1, "Simulating Multiple CLients failing at once (120s).")
# timeline.addAction(clients[len(clients) - 1].die, 120, 1)
# timeline.addAction(clients[len(clients) - 3].die, 120, 1)

#inititateTracker
globals.networktracker.watchTotalNetworkBandwidth()
for client in clients:
    globals.networktracker.watchBandwithForClient(client)

prepareSimFunctions(clients)
for i in range(int(globals.SIMULATION_DURATION / globals.TIME_STEP_SIZE)):
    printTime()
    # printTimeSteps()
    timeline.timeUpdate(globals.TIME)
    for client in clients:
        client.timeUpdate()
    runSimFunctions(globals.TIME)
    for packet in globals.TRAVELING_PACKETS:
        if packet.receiveTime <= globals.TIME:
            try:
                packet.receive()
                # globals.logger.log(packet.targetClient.ip + ": Packet arrived: ", packet.type, packet.senderClient.ip)
            except Exception as e:
                globals.logger.log(e)
            finally:
                globals.TRAVELING_PACKETS.remove(packet)
    globals.networktracker.timeUpdate()
    globals.TIME += globals.TIME_STEP_SIZE

globals.logger.log("Successfully finished Simulation.")
