from logging import NetworkTrafficLogger, Logger

# Config
PACKET_LOSS_PROBABILLITY = 5 #in %
TRAVEL_TIME = 0.004 # Travel time for packets in the Network
MIN_TRAVEL_TIME = 0.002
MAX_TRAVEL_TIME = 0.006
TIME_STEP_SIZE = 0.002
NUM_OF_CLIENTS = 21
SIMULATION_DURATION = 60 #in seconds!
PRINT_LOGS = True
WRITE_FILES = True
LOG_WITH_TIMESTAMP = True

#Constants
ISCP_HEADER_SIZE = 9
SIM_NAME = "Sim5_"

#Vars
PACKETS_SEND = 0
TIME = 0
TRAVELING_PACKETS = []
RUNNING_PROCESSES = []
clients = []
num_of_pass = 1
logger = Logger(SIM_NAME + "actions.txt", writeFile = WRITE_FILES, printLines = PRINT_LOGS)
networktracker = NetworkTrafficLogger(0.0, WRITE_FILES)

#General Logging
logger.initialLog("Starting Simulation of ISCP v1 Network Protocol")
logger.initialLog("Using the following Configuration:")
logger.initialLog("     Sim-Name:", SIM_NAME)
logger.initialLog("     Packet-Loss:", PACKET_LOSS_PROBABILLITY)
logger.initialLog("     Number of Clients (Brutto):", NUM_OF_CLIENTS)