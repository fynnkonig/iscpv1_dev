import globals
import math

class Timeout(Exception):
    pass

def printTime():
    if math.floor(globals.TIME) != math.floor(globals.TIME - globals.TIME_STEP_SIZE):
        print("Time", math.floor(globals.TIME), "s")

def printTimeSteps():
    print(globals.TIME)