<?php
$server = "";
$user = "";
$password = "";
$db_name = "";
$conn = connectToDatabase($server, $user, $password, $db_name);
  
function escape($string) {
    global $conn;
    $string = htmlentities($string);
    return mysqli_real_escape_string($conn, $string);
}

function connectToDatabase($server, $user, $password, $db_name){
    $conn = mysqli_connect($server, $user, $password, $db_name);
    if (!$conn) {
        die("Schwerwiegender Fehler! Daten konnten nicht übermittelt werden. Bitte wende dich an Fynn König! " . mysqli_connect_error());
    }
    return $conn;
}

if(isset($_POST["device"]) && isset($_POST["age"]) && isset($_POST["sex"]) && isset($_POST["notice"]) && isset($_POST["11"]) && isset($_POST["12"]) && isset($_POST["13"]) && isset($_POST["14"]) && isset($_POST["15"]) && isset($_POST["16"]) && isset($_POST["21"]) && isset($_POST["22"]) && isset($_POST["23"]) && isset($_POST["31"]) && isset($_POST["32"]) && isset($_POST["33"]) && isset($_POST["34"]) && isset($_POST["35"]) && isset($_POST["41"]) && isset($_POST["42"]) && isset($_POST["43"]) && isset($_POST["44"]) ){
    $device = escape($_POST["device"]);
    $age = (int)escape($_POST["age"]);
    $sex = escape($_POST["sex"]);
    $notice = escape($_POST["notice"]);
    $_1_1 = (int)escape($_POST["11"]);
    $_1_2 = (int)escape($_POST["12"]);
    $_1_3 = (int)escape($_POST["13"]);
    $_1_4 = (int)escape($_POST["14"]);
    $_1_5 = (int)escape($_POST["15"]);
    $_1_6 = (int)escape($_POST["16"]);
    $_2_1 = (int)escape($_POST["21"]);
    $_2_2 = (int)escape($_POST["22"]);
    $_2_3 = (int)escape($_POST["23"]);
    $_3_1 = (int)escape($_POST["31"]);
    $_3_2 = (int)escape($_POST["32"]);
    $_3_3 = (int)escape($_POST["33"]);
    $_3_4 = (int)escape($_POST["34"]);
    $_3_5 = (int)escape($_POST["35"]);
    $_4_1 = (int)escape($_POST["41"]);
    $_4_2 = (int)escape($_POST["42"]);
    $_4_3 = (int)escape($_POST["43"]);
    $_4_4 = (int)escape($_POST["44"]);
}else{
    die("Ein Fehler ist aufgetreten. Bitte fülle alle Felder aus und versuche es erneut!");
}
if(isset($_POST["mt"])){
    $mt = escape($_POST["mt"]);
}else{
    $mt = "--";
}
if(isset($_POST["note"])){
    $note = escape($_POST["note"]);
}else{
    $note = "";
}
$query = "INSERT INTO `audiotest_results`(`Device`, `Mt`, `Notes`, `Age`, `Sex`, `Notice`, `1_1`, `1_2`, `1_3`, `1_4`, `1_5`, `1_6`, `2_1`, `2_2`, `2_3`, `3_1`, `3_2`, `3_3`, `3_4`, `3_5`, `4_1`, `4_2`, `4_3`, `4_4`) VALUES ('$device','$mt','$note','$age','$sex','$notice','$_1_1','$_1_2','$_1_3','$_1_4','$_1_5','$_1_6','$_2_1','$_2_2','$_2_3','$_3_1','$_3_2','$_3_3','$_3_4','$_3_5','$_4_1','$_4_2','$_4_3','$_4_4')";
$result = mysqli_query($conn, $query);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Befragung zur Nutzbarkeit von Audiosignalen</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,500;0,700;1,400&display=swap" rel="stylesheet">
    <style>
        .wrapper p{
            text-align: justify;
        }
        .wrapper{
            height: 100%;
            box-sizing: border-box;
            padding: 1em;
        }
        .row{
            margin-top: 2em;
            margin-bottom: 2em;
            padding-top: 2em;
            padding-bottom: 0em;
            border-top: 1px solid rgba(151, 93, 93, 0.15);
        }
        body{
            background: rgb(255,255,255);
            background: radial-gradient(circle, rgba(255,255,255,1) 0%, rgba(204,217,219,1) 100%);
            width: 100%;
            height: 100vh;
            margin: 0;
            font-family: 'Montserrat', sans-serif;
            line-height: 1.5em;
        }
        form{
            background-color: rgba(255,255,255,0.6);
            padding: 2em;
            padding-top: 1em;
            padding-bottom: 1em;
            border-radius: 2em 0 2em 2em;
            margin-top: 50%;
        }
        h1{
            text-align: center;
            line-height: 1.5em;
        }
        @media (min-width: 600px){
            .wrapper{
            max-width: 720px;
            margin: auto;
            padding: 5em;
            }
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <form>
            <row>
                <h1>Danke für deine Hilfe!</h1>
            </row>
        </form>
    </div>
</body>
</html>